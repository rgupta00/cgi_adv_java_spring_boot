package com.demo;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Component
@Aspect
class AudianceAspect {
	//@Before, @After @AfterReturing, @AfterThrowing, @Aroud (@Transaction spring tx)
	
	@Around("execution(public void doMagic())")
	public Object aroundAdvice(ProceedingJoinPoint pjp) throws Throwable {
		System.out.println("clapping around advice");
		Object value=pjp.proceed();
		System.out.println("name of mathod :"+pjp.getSignature().getName());
		return value;
	}
	
	
//	@AfterReturning("execution(public void doMagic())")
//	public void clapping() {
//		System.out.println("clapping");
//	}
//	
//	@AfterThrowing("execution(public void doMagic())")
//	public void clappingAfterThrowsing() {
//		System.out.println("call the dr.");
//	}

}