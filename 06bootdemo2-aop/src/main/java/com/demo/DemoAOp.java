package com.demo;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class DemoAOp {

	public static void main(String[] args) {
		
		AnnotationConfigApplicationContext ctx=new AnnotationConfigApplicationContext(AppConfig.class);
		Magician magician=ctx.getBean("magician", Magician.class);
		magician.doMagic();	
		
//		ApplicationContext ctx=new ClassPathXmlApplicationContext("aopdemo.xml");
//		Magician magician=ctx.getBean("magician", Magician.class);
//		magician.doMagic();
		
	}
}
