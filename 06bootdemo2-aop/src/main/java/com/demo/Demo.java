package com.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Demo {
	private static Logger logger=LoggerFactory.getLogger(Demo.class);
	
	public static void main(String[] args) {
		String data="22x";
		try {
			Integer val=Integer.parseInt(data);
			logger.info("the value is :"+val);
		}catch(Exception ex) {
			logger.error("no formate errror");
		}
	}

}
