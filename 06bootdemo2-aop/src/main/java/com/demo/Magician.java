package com.demo;

import org.springframework.stereotype.Component;

@Component(value = "magician")
 public class Magician {
	
	public void doMagic() {
		System.out.println("abra ka dabra...");
		if(1==2)
			throw new RuntimeException("getting high fever! ");
		System.out.println("abra ka dabra.is done.");
	}
}
