package com.bankapp.model.service.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.bankapp.model.service.AccountServiceImpl;

@Component
@Aspect
public class LoggingAspect {
	private Logger logger=LoggerFactory.getLogger(AccountServiceImpl.class);
	
	@Around("@annotation(MyLogging)")
	public void logging(ProceedingJoinPoint pjp) throws Throwable {
		long start=System.currentTimeMillis();
		pjp.proceed();
		long end= System.currentTimeMillis();
		logger.info("time taken to run tranfer is"+ (end-start));
	}
}
