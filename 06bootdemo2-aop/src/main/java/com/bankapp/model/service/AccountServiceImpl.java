package com.bankapp.model.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.bankapp.model.dao.Account;
import com.bankapp.model.dao.AccountDao;
import com.bankapp.model.dao.AccountDaoImpl;
import com.bankapp.model.service.aspect.MyLogging;

@Service(value = "accountService")
public class AccountServiceImpl implements AccountService {

	private AccountDao accountDao;
	private EmailService emailService;

	@Override
	public List<Account> getAll() {

		if (emailService != null) {
			emailService.sendEmail("rgupta.mtech@gmail.com");
		}
		return accountDao.getAll();
	}

	@Autowired
	public void setEmailService(EmailService emailService) {
		this.emailService = emailService;
	}

	@Autowired
	public AccountServiceImpl(AccountDao accountDao) {
		this.accountDao = accountDao;
	}

	@MyLogging
	@Override
	public void transfer(int from, int to, double amount) {

		Account fromAcc = accountDao.getById(from);
		Account toAcc = accountDao.getById(to);

		fromAcc.setBalance(fromAcc.getBalance() - amount);
		toAcc.setBalance(toAcc.getBalance() + amount);

		accountDao.update(fromAcc);
		accountDao.update(toAcc);

		if (emailService != null) {
			emailService.sendEmail("rgupta.mtech@gmail.com");
		}

	}

	@Override
	public void deposit(int id, double amount) {
		Account accountToDeposit = accountDao.getById(id);
		accountToDeposit.setBalance(accountToDeposit.getBalance() + amount);
		accountDao.update(accountToDeposit);
	}

	@Override
	public void withdraw(int id, double amount) {
		Account accountToWithdraw = accountDao.getById(id);
		accountToWithdraw.setBalance(accountToWithdraw.getBalance() - amount);
		accountDao.update(accountToWithdraw);
	}

	@Override
	public Account getById(int id) {
		return accountDao.getById(id);
	}

}
