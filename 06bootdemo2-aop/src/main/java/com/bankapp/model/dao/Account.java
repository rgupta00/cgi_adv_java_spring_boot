package com.bankapp.model.dao;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
@Data
@Builder
@AllArgsConstructor
public class Account {
	private int id;
	private String name;
	private double balance;

}
