package com.empapp.controller;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.empapp.dto.EmpDeptResponse;
import com.empapp.entities.Employee;
import com.empapp.service.EmployeeService;

@RestController
@RequestMapping(path = "api/v1")
public class EmployeeController {

	@Autowired
	private EmployeeService employeeService;
	

	@GetMapping(path = "employee")
	public List<Employee> getAllEmp() {
		List<Employee>employees=employeeService.getAllEmployees();
		return employees;
	}
	
	@GetMapping(path = "empinfo")
	public List<EmpDeptResponse> getAllEmpWihtDeptName() {
		List<EmpDeptResponse>employees=employeeService.getEmployeeDeptInformation();
		return employees;
	}
}
