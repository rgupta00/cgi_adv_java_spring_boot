package com.empapp.controller;
import java.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.empapp.dto.DepartmentDto;
import com.empapp.entities.Department;
import com.empapp.service.DepartmentService;

@RestController
@RequestMapping(path = "api/v1")
public class DepartmentController {

	@Autowired
	private DepartmentService departmentService;
	
	@GetMapping(path = "departmentwithemployees")
	public List<Department> getAllDepts(){
		return departmentService.getAllDeptWithEmployees();
	}
	
	@GetMapping(path = "departmentwithemployees/{did}")
	public Department getAnDepts(@PathVariable(name="did") int did){
		return departmentService.getDepartmentById(did);
	}
	
	@GetMapping(path = "department")
	public List<DepartmentDto> getAllDeptsWithoutEmployees(){
		return departmentService.getAllDeptWithoutEmployee();
	}
	
	
}
