package com.empapp.dto;

public class EmpDeptResponse {
	private String ename;
	private double esalary;
	private String dname;
	
	
	public String getEname() {
		return ename;
	}
	public void setEname(String ename) {
		this.ename = ename;
	}
	public double getEsalary() {
		return esalary;
	}
	public void setEsalary(double esalary) {
		this.esalary = esalary;
	}
	public String getDname() {
		return dname;
	}
	public void setDname(String dname) {
		this.dname = dname;
	}
	public EmpDeptResponse(String ename, double esalary, String dname) {
		this.ename = ename;
		this.esalary = esalary;
		this.dname = dname;
	}
	public EmpDeptResponse() {}
	@Override
	public String toString() {
		return "EmpDeptResponse [ename=" + ename + ", esalary=" + esalary + ", dname=" + dname + "]";
	}
	
	
	
}
