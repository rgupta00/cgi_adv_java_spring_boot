package com.empapp.dto;

public class DepartmentDto {
	private int did;
	private String dname;
	public int getDid() {
		return did;
	}
	public void setDid(int did) {
		this.did = did;
	}
	public String getDname() {
		return dname;
	}
	public void setDname(String dname) {
		this.dname = dname;
	}
	public DepartmentDto(int did, String dname) {
		this.did = did;
		this.dname = dname;
	}
	public DepartmentDto() {}
	
	
	
	
}
