package com.empapp.dto;

public class EmployeeDataResponse {
	private String name;
	private double salary;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getSalary() {
		return salary;
	}
	public void setSalary(double salary) {
		this.salary = salary;
	}
	public EmployeeDataResponse(String name, double salary) {
		System.out.println(name);
		this.name = name;
		this.salary = salary;
	}
	public EmployeeDataResponse() {}
	@Override
	public String toString() {
		return "EmployeeDataResponse [name=" + name + ", salary=" + salary + "]";
	}
	
	
	
}
