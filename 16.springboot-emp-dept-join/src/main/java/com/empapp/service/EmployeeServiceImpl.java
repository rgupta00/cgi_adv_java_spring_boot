package com.empapp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.empapp.dto.EmpDeptResponse;
import com.empapp.dto.EmployeeDataResponse;
import com.empapp.entities.Department;
import com.empapp.entities.Employee;
import com.empapp.exceptions.ResouceNotFoundException;
import com.empapp.repo.DepartmentRepo;
import com.empapp.repo.EmployeeRepo;
@Service
@Transactional
public class EmployeeServiceImpl implements EmployeeService{

	private EmployeeRepo employeeRepo;
	
	private DepartmentRepo deptRepo;
	
	
	
	public EmployeeServiceImpl(EmployeeRepo employeeRepo, DepartmentRepo deptRepo) {
		super();
		this.employeeRepo = employeeRepo;
		this.deptRepo = deptRepo;
	}

	@Override
	public List<Employee> getAllEmployees() {
		return employeeRepo.findAll();
	}

	@Override
	public Employee addEmployee(int deptId, Employee employee) {
		Department department=deptRepo.findById(deptId).orElseThrow
				(()-> new ResouceNotFoundException("dept with id :"+ deptId +"is not found"));
		department.getEmployees().add(employee);
		deptRepo.save(department);
		employee.setDepartment(department);
		
		Employee employee2= employeeRepo.save(employee);
		
		return employee2;
	}

	@Override
	public Employee deleteEmployee(int eid) {
		Employee empToDelete=getEmployeeById(eid);
		employeeRepo.delete(empToDelete);
		return empToDelete;
	}

	@Override
	public Employee updateEmployee(int eid, Employee employee) {
		Employee empToUpdate=getEmployeeById(eid);
		empToUpdate.setSalary(employee.getSalary());
		employeeRepo.save(empToUpdate);
		return empToUpdate;
	}

	@Override
	public Employee getEmployeeById(int eid) {
		Employee employee=employeeRepo.findById(eid).orElseThrow(()-> new ResouceNotFoundException("emp with id :"+ eid +"is not found"));
		return employee;
	}

	@Override
	public List<EmployeeDataResponse> getEmployeeSelectedData() {
		return employeeRepo.getEmployeeSelectedData();
	}

	@Override
	public List<Employee> findEmployeessByIds(List<Integer> eid) {
		return employeeRepo.findEmployeessByIds(eid);
	}

	@Override
	public List<EmpDeptResponse> getEmployeeDeptInformation() {
		return deptRepo.getEmployeeDeptInformation();
	}

}
