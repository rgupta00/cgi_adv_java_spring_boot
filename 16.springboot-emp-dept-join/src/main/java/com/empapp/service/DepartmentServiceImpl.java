package com.empapp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.empapp.dto.DepartmentDto;
import com.empapp.entities.Department;
import com.empapp.exceptions.ResouceNotFoundException;
import com.empapp.repo.DepartmentRepo;
@Service
@Transactional
public class DepartmentServiceImpl implements DepartmentService{

	private DepartmentRepo deptRepo;
	
	@Autowired
	public DepartmentServiceImpl(DepartmentRepo deptRepo) {
		this.deptRepo = deptRepo;
	}

	@Override
	public List<Department> getAllDepartment() {
		return deptRepo.findAll();
	}

	@Override
	public Department addDepartment(Department dept) {
		deptRepo.save(dept);
		return dept;
	}

	@Override
	public Department deleteDepartment(int did) {
		Department deptToDelete=getDepartmentById(did);
		deptRepo.delete(deptToDelete);
		return deptToDelete;
	}

	@Override
	public Department updateDepartment(int did, Department department) {
		Department deptToUpdate=getDepartmentById(did);
		deptToUpdate.setDname(department.getDname());
		deptRepo.save(deptToUpdate);
		return deptToUpdate;
	}

	@Override
	public Department getDepartmentById(int did) {
		Department dept=deptRepo.findById(did)
				.orElseThrow(()-> new ResouceNotFoundException("dept with id: "+ did + " not found"));
		return dept;
	}

	@Override
	public List<Department> getAllDeptWithEmployees() {
		List<Department> departments=deptRepo.getAllDeptWithEmployees();
		return departments;
	}

	@Override
	public List<String> getAllDeptNames() {
		return deptRepo.getAllDeptNames();
	}

	@Override
	public List<DepartmentDto> getAllDeptWithoutEmployee() {
		return deptRepo.getAllDeptWithoutEmployee();
	}

}
