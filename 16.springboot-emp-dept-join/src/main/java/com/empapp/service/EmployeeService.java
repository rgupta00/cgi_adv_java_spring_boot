package com.empapp.service;

import java.util.*;

import org.springframework.data.repository.query.Param;

import com.empapp.dto.EmpDeptResponse;
import com.empapp.dto.EmployeeDataResponse;
import com.empapp.entities.Employee;

public interface EmployeeService {
	public List<Employee> getAllEmployees();

	public Employee addEmployee(int deptId, Employee employee);

	public Employee deleteEmployee(int eid);

	public Employee updateEmployee(int eid, Employee employee);

	public Employee getEmployeeById(int eid);
	
	List<EmployeeDataResponse>getEmployeeSelectedData();
	
	List<Employee> findEmployeessByIds(@Param("eid") List<Integer> eid);

	public List<EmpDeptResponse> getEmployeeDeptInformation();
}
