package com.empapp.service;

import java.util.List;

import com.empapp.dto.DepartmentDto;
import com.empapp.entities.Department;

public interface DepartmentService {
	public List<Department> getAllDepartment();

	public Department addDepartment(Department dept);

	public Department deleteDepartment(int did);

	public Department updateDepartment(int did, Department department);

	public Department getDepartmentById(int did);
	
	
	public List<Department> getAllDeptWithEmployees();
	
	public List<String> getAllDeptNames();
	
	public List<DepartmentDto> getAllDeptWithoutEmployee();
	
}
