package com.empapp.repo;
import java.util.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.empapp.dto.DepartmentDto;
import com.empapp.dto.EmpDeptResponse;
import com.empapp.entities.Department;

@Repository
public interface DepartmentRepo extends JpaRepository<Department,Integer > {

	@Query("select new com.empapp.dto.EmpDeptResponse( e.name, e.salary, d.dname) from Department d inner join d.employees e")
	public List<EmpDeptResponse> getEmployeeDeptInformation();

	@Query("from Department d join fetch d.employees Employee")
	public List<Department> getAllDeptWithEmployees();
	@Query("select d.dname from Department d")
	public List<String> getAllDeptNames();
	
	
	@Query("select new com.empapp.dto.DepartmentDto(d.id, d.dname) from Department d")
	public List<DepartmentDto> getAllDeptWithoutEmployee();
}
