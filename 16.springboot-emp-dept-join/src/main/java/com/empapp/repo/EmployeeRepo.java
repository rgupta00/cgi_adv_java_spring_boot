package com.empapp.repo;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.empapp.dto.EmployeeDataResponse;
import com.empapp.entities.Employee;

@Repository
public interface EmployeeRepo extends JpaRepository<Employee, Integer>{

	@Query("select new com.empapp.dto.EmployeeDataResponse(e.name, e.salary) from Employee e")
	public List<EmployeeDataResponse>getEmployeeSelectedData();
	
	//this can used to fetch multiple emp in one go 3,4,5
	
	@Query("SELECT e FROM Employee e WHERE e.eid IN (:eid)")
	public List<Employee> findEmployeessByIds(@Param("eid") List<Integer> eid);
}








