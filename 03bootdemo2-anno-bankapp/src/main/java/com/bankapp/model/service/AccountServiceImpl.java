package com.bankapp.model.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.bankapp.model.dao.Account;
import com.bankapp.model.dao.AccountDao;
import com.bankapp.model.dao.AccountDaoImpl;

@Service(value = "accountService")
public class AccountServiceImpl implements AccountService {

	private AccountDao accountDao;
	private EmailService emailService;

	// when to go for ctr vs setter injection
	// if dependency is mandetory then go for ctr inject
	// it is is optional go for setter injection
	@Override
	public List<Account> getAll() {

		if(emailService!=null) {
			emailService.sendEmail("rgupta.mtech@gmail.com");
		}
		return accountDao.getAll();
	}

	@Autowired
	public void setEmailService(EmailService emailService) {
		this.emailService = emailService;
	}

	@Autowired
	public AccountServiceImpl(AccountDao accountDao) {
		this.accountDao = accountDao;
	}

	@Override
	public void transfer(int from, int to, double amount) {
		Account fromAcc = accountDao.getById(from);
		Account toAcc = accountDao.getById(to);

		fromAcc.setBalance(fromAcc.getBalance() - amount);
		toAcc.setBalance(toAcc.getBalance() + amount);

		accountDao.update(fromAcc);
		accountDao.update(toAcc);
		
		if(emailService!=null) {
			emailService.sendEmail("rgupta.mtech@gmail.com");
		}
	}

	@Override
	public void deposit(int id, double amount) {
		Account accountToDeposit = accountDao.getById(id);
		accountToDeposit.setBalance(accountToDeposit.getBalance() + amount);
		accountDao.update(accountToDeposit);
	}

	@Override
	public void withdraw(int id, double amount) {
		Account accountToWithdraw = accountDao.getById(id);
		accountToWithdraw.setBalance(accountToWithdraw.getBalance() - amount);
		accountDao.update(accountToWithdraw);
	}

	@Override
	public Account getById(int id) {
		return accountDao.getById(id);
	}

}
