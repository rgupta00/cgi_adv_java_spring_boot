package com.bankapp.model.service;

import java.util.List;

import com.bankapp.model.dao.Account;

public interface AccountService {
	public List<Account> getAll();
	public void transfer(int from, int to , double amount);
	public void deposit(int id, double amount);
	public void withdraw(int id, double amount);
	public Account getById(int id);
}
