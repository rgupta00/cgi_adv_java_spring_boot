package com.bookapp.model.dao;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Book {
	private int id;
	private String isbn;
	private String title;
	private String author;
	private double price;

	//getter setter , ctr
}