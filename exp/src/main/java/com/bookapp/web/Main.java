package com.bookapp.web;

import com.bookapp.config.AppConfig;
import com.bookapp.model.dao.Book;
import com.bookapp.model.service.BookService;
import com.bookapp.model.service.BookServiceImpl;
import com.bookapp.model.service.aspect.AppLogger;

import java.util.*;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
public class Main {

	public static void main(String[] args) {
		AnnotationConfigApplicationContext ctx=new AnnotationConfigApplicationContext(AppConfig.class);
		
		BookService bookService=ctx.getBean("bs", BookService.class);
		List<Book> books=bookService.getAllBooks();
		books.forEach(b-> System.out.println(b));
		
		
	}
}
