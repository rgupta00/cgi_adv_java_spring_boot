package com.empapp.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
@Entity
@Table(name = "dept_table")
public class Department {
	
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private int did;
	
	private String dname;

	@OneToMany(mappedBy = "department", cascade = CascadeType.ALL)
	private List<Employee>employees=new ArrayList<>();

	
	
	
	
	
	public int getDid() {
		return did;
	}

	public void setDid(int did) {
		this.did = did;
	}

	public String getDname() {
		return dname;
	}

	public void setDname(String dname) {
		this.dname = dname;
	}

	public List<Employee> getEmployees() {
		return employees;
	}

	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}

	public Department(String dname) {
		this.dname = dname;
	}

	public Department() {}
	
}
