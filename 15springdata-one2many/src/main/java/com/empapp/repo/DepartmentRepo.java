package com.empapp.repo;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.empapp.dto.EmpDataResponse;
import com.empapp.entities.Department;
/*
 * ename, esalary, deptname
 * 
 * 1. u need to write a dto so that u can hold the data
 * 
 */
@Repository
public interface DepartmentRepo extends JpaRepository<Department, Integer> {
	//public EmpDataResponse(String ename, String deptname, String esalary)
	@Query("SELECT new com.empapp.dto.EmpDataResponse(e.name,d.dname, e.salary) FROM Department d INNER JOIN d.employees e")
	public List<EmpDataResponse> getAllEmpData();
	
}
