package com.empapp.dto;

public class EmpDataResponse {
	private String ename;
	private double  esalary;
	private String deptname;

	public String getEname() {
		return ename;
	}

	public void setEname(String ename) {
		this.ename = ename;
	}


	public double getEsalary() {
		return esalary;
	}

	public void setEsalary(double esalary) {
		this.esalary = esalary;
	}

	public String getDeptname() {
		return deptname;
	}

	public void setDeptname(String deptname) {
		this.deptname = deptname;
	}

	public EmpDataResponse(String ename, String deptname, double esalary) {
		this.ename = ename;
		this.esalary = esalary;
		this.deptname = deptname;
	}

	public EmpDataResponse() {}

	@Override
	public String toString() {
		return "EmpDataResponse [ename=" + ename + ", esalary=" + esalary + ", deptname=" + deptname + "]";
	}
	
	

}
