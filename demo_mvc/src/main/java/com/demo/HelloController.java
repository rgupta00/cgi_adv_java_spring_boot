package com.demo;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;


//adv of spring controller : they are testable and support di, aop and other modules of  spring framework
@Controller
public class HelloController {

	@GetMapping("hello")
	public ModelAndView sayHello() {
		ModelAndView mv=new ModelAndView();//model and view
										//result of business processing view means logical name of jsp
		mv.setViewName("mypage");
		mv.addObject("key", "cgi spring mvc hello app");
		return mv;
	}
}
