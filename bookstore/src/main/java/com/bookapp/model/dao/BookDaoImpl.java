package com.bookapp.model.dao;
import java.util.*;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;

public class BookDaoImpl implements BookDao{

	private EntityManagerFactory emf;
	
	
	public BookDaoImpl() {
		emf=MyEntityManagerFactory.getEntityManagerFactory();
	}

	@Override
	public List<Book> getAllBooks() {
		EntityManager em=emf.createEntityManager();
		TypedQuery<Book> query=em.createQuery("select b from Book b", Book.class);
		List<Book>books=query.getResultList();
		em.close();
		return books;
	}

	@Override
	public Book getBookById(int id) {
		EntityManager em=emf.createEntityManager();
		Book book=em.find(Book.class, id);
		em.close();
		return book;
	}

	@Override
	public Book addBook(Book book) {
		EntityManager em=emf.createEntityManager();
		EntityTransaction tx=em.getTransaction();
		try {
			tx.begin();
			em.persist(book);
			
			tx.commit();
		}catch(Exception ex) {
			tx.rollback();
		}
		em.close();
		return book;
	}

	@Override
	public List<Book> getBookByTitle(String title) {
		return null;
	}

	@Override
	public Book updateBook(int bookId, Book book) {
		EntityManager em=emf.createEntityManager();
		EntityTransaction tx=em.getTransaction();
		try {
			tx.begin();
			em.merge(book);
			tx.commit();
		}catch(Exception ex) {
			tx.rollback();
		}
		em.close();
		return book;
	}

	@Override
	public Book deleteBook(int bookId) {
		EntityManager em=emf.createEntityManager();
		EntityTransaction tx=em.getTransaction();
		Book book=null;
		try {
			tx.begin();
			book=getBookById(bookId);
			em.remove(book);
			tx.commit();
		}catch(Exception ex) {
			tx.rollback();
		}
		em.close();
		return book;
	}

}
