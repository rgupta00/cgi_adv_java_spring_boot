package com.bookapp.model.dao;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class MyEntityManagerFactory {
	private static EntityManagerFactory emf = Persistence.createEntityManagerFactory("cgi_app");
	private MyEntityManagerFactory() {}
	public static EntityManagerFactory getEntityManagerFactory() {
		return emf;
	}
}
