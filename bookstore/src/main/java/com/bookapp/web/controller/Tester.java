package com.bookapp.web.controller;

import com.bookapp.model.dao.Book;
import com.bookapp.model.service.BookService;
import com.bookapp.model.service.BookServiceImpl;

public class Tester {

	public static void main(String[] args) {
		BookService bookService=new BookServiceImpl();
		Book book1=new Book("121AZ", "java basics", "raj", 700);
		Book book2=new Book("1233M", "spring basics", "gunika", 600);
		Book book3=new Book("891AZ", "life skills", "ekta", 750);
		
		bookService.addBook(book1);
		bookService.addBook(book2);
		bookService.addBook(book3);
		
		
		
				
	}
}
