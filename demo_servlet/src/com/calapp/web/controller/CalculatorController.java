package com.calapp.web.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class CalculatorController extends HttpServlet {
	private static final long serialVersionUID = 1L;
   
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Integer xVal=Integer.parseInt(request.getParameter("x"));
		Integer yVal=Integer.parseInt(request.getParameter("y"));
		PrintWriter out=response.getWriter();
		Integer sum=xVal+yVal;
		out.print(sum);
		
	}

}
