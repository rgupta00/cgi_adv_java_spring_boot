
public interface Foo
	{
  	 	default boolean equals(Object o)
   	 	{
      		return false;
   		}
	}
A) Code will not compile 
B) Code with compile
C) Code compile but run time error
D) none of these




Which one of the following abstract methods does not take any argument but
returns a value?
A.The accept() method in java.util.function.Consumer<T> interface
B.The get() method in java.util.function.Supplier<T> interface
C.The test() method in java.util.function.Predicate<T> interface
D.The apply() method in java.util.function.Function<T, R> interface


Choose the best option based on this program:
import java.util.*;
 

class Sort {
	public static void main(String[] args) {
		List<String> strings = Arrays.asList("eeny ", "meeny ", "miny ", "mo");
		Collections.sort(strings, (str1, str2) -> str2.compareTo(str1));
		strings.forEach(string -> System.out.print(string));
	}
}

A.	 Compiler error: improper lambda function definition
B.	This program prints: eeny meeny miny mo
C.	This program prints: mo miny meeny eeny
D.	This program will compile fine, and when run, will crash by throwing a runtime
exception.


Consider code snippet :

class Foo{
 void method() {
          int  cnt = 16;
        Runnable r = new Runnable() {
            public void run() {
                System.out.println("count: " +  cnt );
            }
        };
        Thread t = new Thread(r);
        t.start(); 
         cnt ++;  
} 
}
 A. Code will not compile 
B. Code with compile
C. Code compile but run time error
D. none of these




Behavior parameterization is
1. passing code to methods  arguments
2. passing object to method arguments
3. passing both code and objects to method arguments
4. none of these


Consider code snippet, what would be the output?
List<String>list=Arrays.asList(“java”,”is”,”fun”);
Stream<String> s=list.stream();
Sysout(s.foreach(s->System.out::println());
Sysout(s.foreach(s->System.out::println());

A. Code will not compile 
B. java is fun statement is printed twice
C. java is fun statement is printed once only
D. Code fails at run time


In java 8, R apply(T t) is a method of-
a.Function
b.Process
c.Predicate
d.None

What is Predicate in Java 8 -
a.method
b.class
c.Interface
d.Framework





We need to override which Predicate method in Java 8 -
a.predict(T t)
b.predictable(T t)
c.testable(T t)
d.test(T t)





public class Person{
public String name;
public static void main(String str[]){
Person p=null;
System.out.print(p instanceof Person);
}
What is the result of compilation or execution of the code?
    1 Prints false
    2 Prints true
    3 Results in compilation error
    4 Results in runtime error







Consider code :
public class Foof{
public static void main(String str[]){
String s=new String (null);
System.out.print(s);
}
What is the result of compilation or execution of the code?
A. Print null.
B. Print nothing
C. Compilation error
D. Runtime error








public class Demo {
	public static void main(String[] args) {
		System.out.println(returnSomething());
	}
	private static int returnSomething() {
		try{
			return 6;
		}finally{
			return 9;
		}
	}
}

What is the result of compilation or execution of the code?
  A. Prints 9
  B. Results in compilation error
  C. Results in runtime error
  D. Print 6







What will be the result of compiling and running the following code?
class Base{
public short getValue(){ return 1; } //1
}
class Base2 extends Base{
public byte getValue(){ return 2; } //2
}
public class TestClass{
public static void main(String[] args){
Base b = new Base2();
System.out.println(b.getValue()); //3
}
}
Select 1 option
A. It will print 1
B. It will print 2.
C. Compile time error at //1
D. Compile time error at //2







Which of the following are valid classes?

Select 1 option
A. public class ImaginaryNumber extends Number {
}
B. public class ThreeWayBoolean extends Boolean {
}
C. public class NewSystem extends System {
}
D. public class ReverseString extends String {
}


class A {  
public static void f(){
System.out.println(“fA”);   }}

class B extends A{
public void f(){
System.out.println(“fB”);   }
public static void main(String[] args) { 
A a= new B();
a.f();   }}

What will happen on compilation or execution of code?
        A.A fA 
        A.B fB 
        A.C Code will not compile
        A.D Code will throw runtime error







What will be the result of attempting to compile and run the following program?
public class TestClass{
public static void main(String args[ ] ){
A o1 = new C( );
B o2 = (B) o1;
System.out.println(o1.m1( ) );
System.out.println(o2.i );
}
}

class A { int i = 10; int m1( ) { return i; } }
class B extends A { int i = 20; int m1() { return i; } }
class C extends B { int i = 30; int m1() { return i; } }
Select 1 option
A. The program will fail to compile.
B. Class cast exception at runtime.
C. It will print 30, 20.
D. It will print 30, 30.
E. It will print 20, 20.




class Base {
	public static void foo(Base bObj) {
		System.out.println("In Base.foo()");
		bObj.bar();
	}

	public void bar() {
		System.out.println("In Base.bar()");
	}
}

class Derived extends Base {
	public static void foo(Base bObj) {
		System.out.println("In Derived.foo()");
		bObj.bar();
	}

	public void bar() {
		System.out.println("In Derived.bar()");
	}
}

class OverrideTest {
	public static void main(String[] args) {
		Base bObj = new Derived();
		bObj.foo(bObj);
	}
}
What is the output of this program when executed?
a)
In Base.foo()
In Base.bar()
b)
In Base.foo()
In Derived.bar()
c)
In Derived.foo()
In Base.bar()
d)
In Derived.foo()
In Derived.bar()


 Consider following example:
public interface xyz {
void abc() throws IOException;
}
public interface pqr {
void abc() throws FileNotFoundException;
}
public class Implementation implements xyz, pqr {
// insert code
{ /*implementation*/ }
}
Which of the following statement(s) can you insert in place of “// insert code” comment?
A. public void abc() throws IOException
B. public void abc() throws FileNotFoundException
C. public void abc() throws FileNotFoundException, IOException
D. public void abc() throws IOException, FileNotFoundException



 Consider the following program:
class ExceptionTest {
public static void foo() {
try {
throw new ArrayIndexOutOfBoundsException();
} catch(ArrayIndexOutOfBoundsException oob) {
throw new Exception(oob);
}
}
public static void main(String []args) {
try {
foo();
} catch(Exception re) {
System.out.println(re.getCause());
}
}
}

Which one of the following options correctly describes the behavior of this program?
A. java.lang.Exception
B. java.lang.ArrayIndexOutOfBoundsException
C. class java.lang.IllegalStateException
D. T his program fails with compiler error(s)


Which are true? (Choose all that apply.)
A. "X extends Y" is correct if and only if X is a class and Y is an interface.
B. "X extends Y" is correct if and only if X is an interface and Y is a class.
C. "X extends Y" is correct if X and Y are either both classes or both interfaces.
D. "X extends Y" is correct for all combinations of X and Y being classes and/or interfaces.






 Given:
class Rocket {
private void blastOff() { System.out.print("bang "); }
}
public class Shuttle extends Rocket {
public static void main(String[] args) {
new Shuttle().go();
}
void go() {
blastOff();
// Rocket.blastOff(); // line A
}
private void blastOff() { System.out.print("sh-bang "); }
}
Which are true?
A. As the code stands, the output is bang
B. As the code stands, the output is sh-bang
C. As the code stands, compilation fails.
D. If line A is uncommented, the output is bang bang




interface Gadget {
void doStuff();
}
abstract class Electronic {
void getPower() { System.out.print("plug in "); }
}
public class Tablet extends Electronic implements Gadget {
void doStuff() { System.out.print("show book "); }
public static void main(String[] args) {
new Tablet().getPower();
new Tablet().doStuff();
}
}
Which are true? (Choose all that apply.)
A. The class Tablet will NOT compile
B. The interface Gadget will NOT compile
C. The output will be plug in show book
D. The abstract class Electronic will NOT compile






import java.util.*;
class UtilitiesTest {
public static void main(String []args) {
List<int> intList = new ArrayList<>();
intList.add(10);
intList.add(20);
System.out.println("The list is: " + intList);
}
}
A. I t prints the following: The list is: [10, 20].
B. I t prints the following: The list is: [20, 10].
C. It results in a compiler error.
D. I t results in a runtime exception.







Predict the outcome of the following program:
public class ParseString1 {
public static void main(String[] s) {
String quote = "Never lend books-nobody ever returns them!";
String [] words = quote.split("  ", 2);
// split strings based on the delimiter " " (space)
for (String word : words) {
System.out.println(word);
}
}
}
A. It will result in a compile-time error.
B. It will result in a runtime exception.
C. It will print the following output when executed
Never lend
D. It will print the following output when executed
Never 
lend books-nobody ever returns them!


 What is not true about java interface
    1 We can not define instance variable inside Java interface 
    2 We can define constant inside java interface
    3 We can have constructor inside java interface
    4 All are correct



