package com.day1.session2.ex3;
//behavoural parameterization? why to use it?
//SOLID principal, Design pattern?
import java.util.*;
import java.util.function.Predicate;
import static java.util.stream.Collectors.*;
public class AppleApp {
	
	public static  List<Apple> getAllApplesOnCondiation(List<Apple>apples, 
			Predicate<Apple> predicate){
		return apples.stream().filter(predicate).collect(toList());
	}
	
}

