package com.day1.session2.ex3;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

public class AppleTester {

		
		public static void main(String[] args) {

		List<Apple> apples = Arrays.asList(new Apple("red", 400), new Apple(
				"green", 300), new Apple("green", 200), new Apple("red", 250));
		
		
		Predicate<Apple> greenPredicate= apple-> apple.getColor().equals("green");
		Predicate<Apple> HeavyPredicate= apple-> apple.getWeight()>=350;
		
		
		List<Apple> allGreenApples= AppleApp.getAllApplesOnCondiation(apples, greenPredicate.or(HeavyPredicate));
		allGreenApples.forEach(a-> System.out.println(a));
		
		//Most imp functional interface in java 8
		
		//Predicate	
			
		//Function
		
		//Consumer
		
		//biConsumer
		Map<String, Integer>map=new HashMap<String, Integer>();
		
		//Supplier
	
		
		//BiFunction
	
		
		
		
	}
}

















