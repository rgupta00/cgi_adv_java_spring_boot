package com.day1.session1.ex1;
/*
 * stream vs Collection
 * flow of data vs storage to store the data
 * Collection:+ more concepts
 */

//java 9 we can also have private method inside the interface : helper to default method

interface D {
	public default void foo1() {
		fooCommon();
	}

	public default void foo2() {
		fooCommon();
	}

	private void fooCommon() {
		System.out.println("some common code");
	}
}

//java 8

interface A {
	public default void foo() {
		System.out.println("default method of A");
	}
}

interface B {
	public default void foo() {
		System.out.println("default method of B");
	}
}

class C implements A, B {

	@Override
	public void foo() {
		A.super.foo();
		B.super.foo();
	}

}

public class DemoInterfaceDiamondProbl {

	public static void main(String[] args) {

		Foof.foofStatic();
		Foof f = new FoofImp1();
		f.foo2();
	}

}
