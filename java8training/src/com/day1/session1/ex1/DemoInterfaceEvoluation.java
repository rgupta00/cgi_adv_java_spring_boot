package com.day1.session1.ex1;
/*
 * stream vs Collection
 * flow of data vs storage to store the data
 * Collection:+ more concepts
 */

//java 8
interface Foof{
	public void foo();
	public default void foo2() {
		System.out.println("default method...");
	}
	public static void foofStatic() {
		System.out.println("foo static method");
	}
}
//syed
class FoofImp1 implements Foof{

	@Override
	public void foo() {
		System.out.println("foofImp1 is done");
	}
	
	public  void foo2() {
		System.out.println("default method is overren by FoofImp1 class");
	}
}

//TS
class FoofImp2 implements Foof{

	@Override
	public void foo() {
		System.out.println("foofImp1 is done");
	}
	
}

public class DemoInterfaceEvoluation {
	
	public static void main(String[] args) {
		
		Foof.foofStatic();
		Foof f=new FoofImp1();
		f.foo2();
	}

}
