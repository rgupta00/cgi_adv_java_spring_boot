package com.day1.session1.ex2;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
public class DemoLambdaExpession2 {

	
	public static void main(String[] args) {
		
		List<Book> books=Arrays.asList
				(new Book(121, "java", "raj", 240),
						new Book(11, "python", "ekta", 840),
						new Book(621, "c programming", "gunika", 300));
		
		//Collections.sort(books,(Book o1, Book o2)-> Double.compare(o2.getPrice(), o1.getPrice()));
		books.stream()
		.sorted(Comparator.comparing(Book::getPrice).reversed())
		.forEach(b-> System.out.println(b));
		
	}
}
