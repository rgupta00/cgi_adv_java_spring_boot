package com.demo;

public class MySingleton {
	private static MySingleton mySingleton=new MySingleton();
	
	private MySingleton() {
		System.out.println("dare to call me");
	}
	
	public static MySingleton getMySingleton() {
		return mySingleton;
	}
}
