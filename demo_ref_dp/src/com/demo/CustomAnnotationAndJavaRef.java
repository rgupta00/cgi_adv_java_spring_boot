package com.demo;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Method;

//how to create ur own annotation (aka mata data)
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@interface MyAnnotation{
	public String hello() default "cgi training";
	public boolean isDone() default false;
}

//how to apply this
class MyClass{
	@MyAnnotation(hello = "hello to cgi peoples" , isDone = false)
	public void myMethod() {
		System.out.println("some business logic");
	}
}
// u need process it : annotation processing : hibernate , spring they all use it internally!
//how to process it : java reflection !

public class CustomAnnotationAndJavaRef {
	
	public static void main(String[] args) throws ClassNotFoundException {
		Class clazz= Class.forName("com.demo.MyClass");
		
		Method []methods=clazz.getDeclaredMethods();
		
		for(Method method: methods) {
			if(method.isAnnotationPresent(MyAnnotation.class)) {
				MyAnnotation annotation =method.getAnnotation(MyAnnotation.class);
				System.out.println(annotation.hello());
				System.out.println(annotation.isDone());
			}
		}
		
	}

}





