package com.demo;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

public class DemoRef {

	public static void main(String[] args) throws ClassNotFoundException {
		//till now how many ways we know to create the objects
		
		//static class loading
//		Employee employee=new Employee(12, "raj", 5000);
//		System.out.println(employee);
//		System.out.println(employee.getClass());
//		System.out.println(Employee.class);
		//dynamic class loading
		//Class is a class in java which is used to get reflection know at run time
		
		//Dynamic class loading
		Class class1=Class.forName("com.demo.Employee");
		//from this Class class object i can get reflection knowledge of class Employee
		
		//i want to use reflection to get info of all method of that class
		Method[]methods=class1.getMethods();
		
		for(Method method: methods) {
			System.out.println(method.getName()+" : "+ method.getModifiers()+" : "+ Modifier.toString(method.getModifiers()));
		
		}
		
	}
	
	
}





