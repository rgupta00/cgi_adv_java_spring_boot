package com.demo;
import java.util.*;

/*
 * 			Annotation (aka meta data)
 * 			|
 * _____________________
 * |					|
 * inbuild ann			custom annotation ( u an create ur own annotation) : all framework companies do that
 * @Override
 * 
 */
class A{
	void foo() {
		System.out.println("method of base class");
	}
	@Deprecated
	public void oldApi() {
		System.out.println("some old junk api");
	}
	@SuppressWarnings({"unchecked","rawtypes"})
	public void newApi() {
		System.out.println("new api");
	
		List list=new ArrayList();
		list.add("raj");
	}
}
class B extends  A{
	@Override
	void foo() {
		System.out.println("method of base class is overriden");
		//Deprecated?
	}
}
public class DemoAnnotation {

	public static void main(String[] args) {
		
		
		A a=new A();
		a.oldApi();
		a.newApi();
	}
}
