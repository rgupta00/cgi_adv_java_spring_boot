package com.demo;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class CallilngPrivateCtrUsingJavaRef {

	public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		Class class1= Class.forName("com.demo.MySingleton");
		
		Constructor []constructors=class1.getDeclaredConstructors();
		constructors[0].setAccessible(true);// get me power to call even a private ctr
		
		MySingleton newInstance = (MySingleton) constructors[0].newInstance();
		
		
	}
}
