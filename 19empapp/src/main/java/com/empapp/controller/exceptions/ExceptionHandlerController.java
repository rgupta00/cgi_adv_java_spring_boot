package com.empapp.controller.exceptions;

import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import com.empapp.service.EmployeeNotFoundException;

@RestController
@RestControllerAdvice //using aop throws advice

public class ExceptionHandlerController {

	@ExceptionHandler(Exception.class)
	public ResponseEntity<ErrorDetails> handleOtherExceptions
	(EmployeeNotFoundException ex,  WebRequest req){
		ErrorDetails details=new ErrorDetails(new Date(),"some server side error", 
				req.getDescription(false));
		return new ResponseEntity<ErrorDetails>(details, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	
	
	@ExceptionHandler(EmployeeNotFoundException.class)
	public ResponseEntity<ErrorDetails> handleEmployeeNotFound
	(EmployeeNotFoundException ex,  WebRequest req){
		ErrorDetails details=new ErrorDetails(new Date(),"emp not found", 
				req.getDescription(false));
		return new ResponseEntity<ErrorDetails>(details, HttpStatus.NOT_FOUND);
	}
}
