package com.empapp.controller;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.empapp.dao.Employee;
import com.empapp.service.EmployeeService;

@RestController
@RequestMapping(path = "api/v1")
public class EmployoeeController {

	private EmployeeService employeeService;

	@Autowired
	public EmployoeeController(EmployeeService employeeService) {
		this.employeeService = employeeService;
	}
	
	//ResponeEntity: wrap data + http status code
	
	@GetMapping(path = "employee")
	public ResponseEntity<List<Employee>> getAllEmployees(){
		List<Employee> employees=employeeService.getAllEmployee();
		return new ResponseEntity<List<Employee>>(employees, HttpStatus.OK);
	}
	
	
	@GetMapping(path = "employee/{id}")
	public ResponseEntity<Employee> getAnEmployeeById(@PathVariable(name = "id")  int id){
		return new ResponseEntity<Employee>(employeeService.getEmployeeById(id), HttpStatus.OK);
	}
	
	@GetMapping(path = "employeebyname/{name}")
	public Employee getAnEmployeeByName(@PathVariable(name = "name")  String name){
		return employeeService.getEmployeeByName(name);
	}
	
	//-------adding a emp
	
	//-------updating an emp
	
	//-------- deleting an emp
	
	@PostMapping(path = "employee", 
			produces = MediaType.APPLICATION_JSON_VALUE,
			consumes =MediaType.APPLICATION_JSON_VALUE )
	public ResponseEntity<Employee> addEmployee(@RequestBody Employee employee) {
		Employee employee2= employeeService.addEmployee(employee);
		return new ResponseEntity<Employee>(employee2, HttpStatus.CREATED);
	}
	
	@PutMapping(path = "employee/{id}", 
			produces = MediaType.APPLICATION_JSON_VALUE,
			consumes =MediaType.APPLICATION_JSON_VALUE )
	public Employee updateEmployee(@PathVariable(name = "id")  int id,
			@RequestBody Employee employee) {
		return employeeService.updateEmployee(id, employee);
	}
	
	
	@DeleteMapping(path = "employee/{id}")
	public Employee deleteAnEmployeeById(@PathVariable(name = "id")  int id){
		return employeeService.deleteEmployee(id);
	}
	
}








