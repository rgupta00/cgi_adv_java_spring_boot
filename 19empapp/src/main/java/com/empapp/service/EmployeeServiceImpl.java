package com.empapp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.empapp.dao.Employee;
import com.empapp.dao.EmployeeRepo;
@Service
@Transactional
public class EmployeeServiceImpl implements EmployeeService {

	private EmployeeRepo employeeRepo;
	
	@Autowired
	public EmployeeServiceImpl(EmployeeRepo employeeRepo) {
		this.employeeRepo = employeeRepo;
	}

	@Override
	public List<Employee> getAllEmployee() {
		return employeeRepo.findAll();
	}

	@Override
	public Employee getEmployeeById(int id) {
		return employeeRepo.findById(id)
				.orElseThrow(()-> new EmployeeNotFoundException());

	}

	@Override
	public Employee getEmployeeByName(String name) {
		return employeeRepo.findByName(name);
	}

	@Override
	public Employee addEmployee(Employee employee) {
		employeeRepo.save(employee);
		return employee;
	}

	@Override
	public Employee deleteEmployee(int id) {
		Employee employeeToDelete= getEmployeeById(id);
		employeeRepo.delete(employeeToDelete);
		
		return employeeToDelete;
	}

	@Override
	public Employee updateEmployee(int id, Employee employee) {
		Employee employeeToUpdate= getEmployeeById(id);
		employeeToUpdate.setSalary(employee.getSalary());
		employeeRepo.save(employeeToUpdate);
		
		return employeeToUpdate;
	}

}
