package com.empapp.service;
import java.util.*;

import com.empapp.dao.Employee;
public interface EmployeeService {
	public List<Employee> getAllEmployee();
	public Employee getEmployeeById(int id);
	public Employee getEmployeeByName(String name);
	public Employee addEmployee(Employee employee);
	public Employee deleteEmployee(int id);
	public Employee updateEmployee(int id , Employee employee);

}
