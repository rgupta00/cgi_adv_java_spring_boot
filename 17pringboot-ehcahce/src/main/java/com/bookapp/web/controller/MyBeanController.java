package com.bookapp.web.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bookapp.dao.MyBean;

@RestController
public class MyBeanController {

	@GetMapping(path = "mybean")
	public MyBean getBean() {
		return new MyBean("java", "python", "c++");
		
	}
}
