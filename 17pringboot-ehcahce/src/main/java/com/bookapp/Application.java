package com.bookapp;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.bookapp.dao.Book;
import com.bookapp.service.BookService;
@SpringBootApplication
@EnableCaching //1
@EnableScheduling //1
public class Application implements CommandLineRunner{

	@Autowired
	private BookService bookService;
	
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

//	//2
//	@Bean
//	public CacheManager getCacheManager() {
//		ConcurrentMapCacheManager cacheManager=new ConcurrentMapCacheManager("books");
//		return cacheManager;
//	}
	
	
	
	
	@Override
	public void run(String... args) throws Exception {
		SimpleDateFormat fmt=new SimpleDateFormat("dd/MM/yyyy");
		
		Date date1=fmt.parse("12/10/2019");
		Date date2=fmt.parse("23/2/2018");
		
		
		Book book=new Book("java basics", "raj", 800.0, "pbp", date1);
		Book book2=new Book("spring basics", "sumit", 790.0, "abc", date2);
		
		//bookService.addBook(book);
		//bookService.addBook(book2);
		
		
	}

}
