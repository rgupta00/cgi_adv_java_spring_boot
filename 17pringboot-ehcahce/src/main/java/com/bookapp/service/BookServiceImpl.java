package com.bookapp.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bookapp.dao.Book;
import com.bookapp.dao.BookDao;

@Service
@Transactional
public class BookServiceImpl implements BookService {

	public BookDao bookDao;

	@Autowired
	public BookServiceImpl(BookDao bookDao) {
		this.bookDao = bookDao;
	}

	@Override
	public List<Book> getAllBooks() {
		System.out.println("------getallbooks is called---------");
		return bookDao.getAllBooks();
	}

	@CachePut(value ="books" , key = "#result.id" )
	@Override
	public Book addBook(Book book) {
		System.out.println("------addbook is called---------");
		return bookDao.addBook(book);
	}
	@CachePut(value ="books" , key = "#result.id" )
	@Override
	public Book updateBook(int bookId, Book book) {
		System.out.println("------update is called---------");
		return bookDao.updateBook(bookId, book);
	}

	@CacheEvict(value = "books", key = "#bookId")
	@Override
	public Book deleteBook(int bookId) {
		System.out.println("------delete is called---------");
		return bookDao.deleteBook(bookId);
	}
	//http://localhost:8090/bookapp/api/book/2
	
	@Cacheable(value = "books", key = "#bookId")
	@Override
	public Book getBookById(int bookId) {
		System.out.println("------getbookbyid is called---------");
		return bookDao.getBookById(bookId);
	}

	//hey delete all cached : how to run it after each 30 min ? scheduled services!
	@CacheEvict(value = "books", allEntries = true)
	@Override
	public void cleanCache() {
		System.out.println("cache is cleaned :"+ new Date().toString());
	}

}
