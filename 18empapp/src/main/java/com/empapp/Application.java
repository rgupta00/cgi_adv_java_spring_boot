package com.empapp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.empapp.dao.Employee;
import com.empapp.service.EmployeeService;

@SpringBootApplication
public class Application implements CommandLineRunner{

	@Autowired
	private EmployeeService employeeService;
	
	
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
//		Employee employee1=new Employee("raj", 200.0);
//		Employee employee2=new Employee("ekta",100.0);
//		Employee employee3=new Employee("gunika", 50.0);
//		Employee employee4=new Employee("keshav", 40.0);
//		
//		employeeService.addEmployee(employee1);
//		employeeService.addEmployee(employee2);
//		employeeService.addEmployee(employee3);
//		employeeService.addEmployee(employee4);
//		
//		
//		System.out.println("--------added......");
	}

}
