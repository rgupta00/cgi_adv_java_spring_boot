package com.empapp.controller;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.empapp.dao.Employee;
import com.empapp.service.EmployeeService;

@RestController
@RequestMapping(path = "api/v1")
public class EmployoeeController {

	private EmployeeService employeeService;

	@Autowired
	public EmployoeeController(EmployeeService employeeService) {
		this.employeeService = employeeService;
	}
	
	@GetMapping(path = "employee")
	public List<Employee> getAllEmployees(){
		List<Employee> employees=employeeService.getAllEmployee();
		return employees;
	}
	
	
	@GetMapping(path = "employee/{id}")
	public Employee getAnEmployeeById(@PathVariable(name = "id")  int id){
		return employeeService.getEmployeeById(id);
	}
	
	@GetMapping(path = "employeebyname/{name}")
	public Employee getAnEmployeeByName(@PathVariable(name = "name")  String name){
		return employeeService.getEmployeeByName(name);
	}
	
	//-------adding a emp
	
	//-------updating an emp
	
	//-------- deleting an emp
	
	@PostMapping(path = "employee", 
			produces = MediaType.APPLICATION_JSON_VALUE,
			consumes =MediaType.APPLICATION_JSON_VALUE )
	public Employee addEmployee(@RequestBody Employee employee) {
		return employeeService.addEmployee(employee);
	}
	
	@PutMapping(path = "employee/{id}", 
			produces = MediaType.APPLICATION_JSON_VALUE,
			consumes =MediaType.APPLICATION_JSON_VALUE )
	public Employee updateEmployee(@PathVariable(name = "id")  int id,
			@RequestBody Employee employee) {
		return employeeService.updateEmployee(id, employee);
	}
	
	
	@DeleteMapping(path = "employee/{id}")
	public Employee deleteAnEmployeeById(@PathVariable(name = "id")  int id){
		return employeeService.deleteEmployee(id);
	}
	
}








