package com.demo;
import java.util.*;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class GetVsLoad {

	public static void main(String[] args) {
		
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("cgi_app");
		EntityManager em = emf.createEntityManager();
		Customer customer=em.getReference(Customer.class, 131);
		em.close();
		System.out.println(customer);
		emf.close();

	}

}
