package com.demo;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class AddCustomer {

	public static void main(String[] args) {

		// SessionFactory----------------------> EntityManagerFactory emf
		/// Session --------------EntityManager
		// hibernate.cfg.xml ------------persistance.xml

		EntityManagerFactory emf = Persistence.createEntityManagerFactory("cgi_app");

		EntityManager em = emf.createEntityManager();

		EntityTransaction tx = em.getTransaction();

		try {
			tx.begin();
			Customer customer2 = new Customer(110, "ekta", "55904545");
			Customer customer3 = new Customer(19, "keshav", "55900545");
			Customer customer4 = new Customer(191, "amit", "5804545");
			Customer customer5 = new Customer(131, "sumit", "66904545");

			em.persist(customer2);
			em.persist(customer3);
			em.persist(customer4);
			em.persist(customer5);
			tx.commit();
		
		} catch (Exception ex) {
			tx.rollback();
			ex.printStackTrace();
		}
		em.close();
		emf.close();

	}

}
