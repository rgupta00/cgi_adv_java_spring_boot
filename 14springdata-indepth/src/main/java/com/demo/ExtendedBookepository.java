package com.demo;

import org.springframework.stereotype.Repository;

@Repository
public interface ExtendedBookepository extends ExtendedRepository<Book, Long> {
}