package com.demo;
import java.util.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application implements CommandLineRunner {

	@Autowired
	private BookRepository bookRepo;
	
	@Autowired
	private ExtendedBookepository bookepository;
	
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		
		List<Book>books=bookRepo.allBooks();
		books.forEach(b-> System.out.println(b));
		
		
		
		
		//Book book=bookRepo.findByTitleContaining("effective");
		//List<Book>books=bookRepo.findByPageCountBetween(500, 800);
		//books.forEach(b-> System.out.println(b));
		
		


//		Date date = new SimpleDateFormat("MM/dd/yyyy").parse("10/22/2010");
//			Date date2 = new SimpleDateFormat("MM/dd/yyyy").parse("10/22/2020");
//			for(Book b:bookRepo.findByPublishDateBetween(date,date2)){
//				System.out.println(b);
//			}

		//bookepository.findAll().forEach(b-> System.out.println(b));
//		SimpleDateFormat fmt=new SimpleDateFormat("dd/MM/yyyy");
//		Date date1=fmt.parse("11/11/2012");
//		Date date2=fmt.parse("1/1/2018");
//		Date date3=fmt.parse("1/11/2019");
//		Date date4=fmt.parse("22/2/2021");
//		
//		
//		//Book book=new Book(title, publishDate, pageCount, price)
//		Book book1=new Book("effective java", date1, 500, 590.00);
//		Book book2=new Book("rich dad poor dad", date2, 300, 490.00);
//		Book book3=new Book("spring in action", date3, 560, 700.00);
//		Book book4=new Book("hibernte in action", date4, 670, 900.00);
		
		//bookRepo.deleteAll();
		
		
//		bookRepo.save(book1);
//		bookRepo.save(book2);
//		bookRepo.save(book3);
//		bookRepo.save(book4);
		
		//bulk insertion
		
		//List<Book> bookList=List.of(book1, book2, book3, book4);
		//bookRepo.saveAll(bookList);
		
		//System.out.println(bookRepo.count());
		
		//bookRepo.deleteById(5L);
		//bookRepo.deleteAll(List.of(book1, book2));
		
		//bookRepo.deleteById(5L);
		//bookRepo.deleteAllInBatch();
		
	}

}




