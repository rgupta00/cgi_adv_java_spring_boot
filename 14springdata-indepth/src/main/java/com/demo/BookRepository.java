package com.demo;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
@Repository
public interface BookRepository extends JpaRepository<Book, Long> {
	
	
	public List<Book> allBooks();
	
	@Query("select b from Book b")
	public List<Book> getAllBooks();
	
	@Query("select b from Book b where b.pageCount >?1")
	public List<Book> getAllBooksHavingPagesMoreThen(int pageCount);
	
	@Query("select b from Book b where b.pageCount >:pageCount")
	public List<Book> getAllBooksHavingPagesMoreThenV2(@Param(value ="pageCount")  int pageCount);
	
	//public Book findByTitle(String title);
	//public Book findByTitleLike(String title);
	//public Book findByTitleContaining(String title);
	//public Book findByTitle(String title);
	//public List<Book> findByPublishDateBetween(Date date,Date date2);
	//public List<Book> findByPageCountBetween(int min, int max);
	
}
