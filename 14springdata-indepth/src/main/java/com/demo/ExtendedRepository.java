package com.demo;

import java.io.Serializable;

import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.Repository;
@NoRepositoryBean
public interface ExtendedRepository<T,ID extends Serializable> extends Repository<T, ID> {

	Iterable<T> findAll();
}
