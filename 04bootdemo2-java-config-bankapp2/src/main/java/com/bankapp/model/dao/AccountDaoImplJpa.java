package com.bankapp.model.dao;

import java.util.*;

public class AccountDaoImplJpa implements AccountDao{
	private Map<Integer, Account> accounts=new HashMap<Integer, Account>();
	
	{
		accounts.put(1, new Account(1,"raj",5000));
		accounts.put(2, new Account(2,"ekta",5000));
		
	}
	@Override
	public List<Account> getAll() {
		System.out.println("using jpa");
		return new ArrayList<Account>(accounts.values());
	}

	@Override
	public void update(Account account) {
		accounts.put(account.getId(), account);
	}

	@Override
	public Account getById(int id) {
		return accounts.get(id);
	}

}
