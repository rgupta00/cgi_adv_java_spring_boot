package com.bankapp.web.controller;
import java.util.*;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.bankapp.config.AppConfig;
import com.bankapp.model.dao.Account;
import com.bankapp.model.service.AccountService;
import com.bankapp.model.service.AccountServiceImpl;

public class Main {

	public static void main(String[] args) {
		
		//ApplicationContext ctx=new ClassPathXmlApplicationContext("beans.xml");
		
		AnnotationConfigApplicationContext ctx=new AnnotationConfigApplicationContext(AppConfig.class);
		
		AccountService accountService=ctx.getBean("accountService", AccountService.class);
		
		List<Account> accounts=accountService.getAll();
		accounts.forEach(account-> System.out.println(account));
		
//		//transfer 1-> 2  100
//		//accountService.transfer(1, 2, 100);
//		accountService.deposit(1, 100);
//		 accounts=accountService.getAll();
//		accounts.forEach(account-> System.out.println(account));
	}
}
