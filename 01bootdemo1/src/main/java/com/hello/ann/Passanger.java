package com.hello.ann;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


@Component(value = "p")
public class Passanger {
	
	@Value(value = "rajesh")
	private String name;

	//field , setter and constructor
	@Autowired	
	private Vehical vehical;

	public Passanger() {
		System.out.println("ctr of passanger is called..");
	}
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Vehical getVehical() {
		return vehical;
	}

	public void setVehical(Vehical vehical) {
		this.vehical = vehical;
	}

	public void travel() {
		System.out.println("name of passanger:" + name);
		vehical.move();
	}
}
