package com.hello.ann;

import org.springframework.stereotype.Component;


@Component(value = "metro")
public class Metro implements Vehical{
	public void move() {
		System.out.println("moving in a metro");
	}
}
