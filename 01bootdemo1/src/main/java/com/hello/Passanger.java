package com.hello;
// high level	 low level
//      pull
//Passanger ---> Vehical, OCP

public class Passanger {
	
	private String name;

	private Vehical vehical;

	public Passanger() {
	}
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Vehical getVehical() {
		return vehical;
	}

	public void setVehical(Vehical vehical) {
		this.vehical = vehical;
	}

	public void travel() {
		System.out.println("name of passanger:" + name);
		vehical.move();
	}
}
