package com.hello;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class DemoDI {
	//so that enven i dont have to chage the main
	public static void main(String[] args) {

		ApplicationContext ctx=new ClassPathXmlApplicationContext("bean.xml");
		Passanger passanger=ctx.getBean("passanger", Passanger.class);
		passanger.travel();
		
//		Vehical vehical=new Bike();
//		Passanger passanger=new Passanger();
//		//still it pull!
//		passanger.setName("ravi");
//		passanger.setVehical(vehical);//passanger is pulling the dep
//		passanger.travel();
	}
}
