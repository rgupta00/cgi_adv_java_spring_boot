package com.hello.java_conf;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class DemoDI {
	//so that enven i dont have to chage the main
	public static void main(String[] args) {

		//ApplicationContext ctx=new ClassPathXmlApplicationContext("bean1.xml");
		AnnotationConfigApplicationContext ctx=
				new AnnotationConfigApplicationContext(AppConfig.class);
		
		Passanger passanger=ctx.getBean("p", Passanger.class);
		passanger.travel();
		
//		Vehical vehical=new Bike();
//		Passanger passanger=new Passanger();
//		//still it pull!
//		passanger.setName("ravi");
//		passanger.setVehical(vehical);//passanger is pulling the dep
//		passanger.travel();
	}
}
