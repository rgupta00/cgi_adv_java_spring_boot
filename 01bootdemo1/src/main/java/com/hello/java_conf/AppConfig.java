package com.hello.java_conf;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/*
 * <context:annotation-config/> <!-- hey spring i will go for anntation -->
	<context:component-scan base-package="com.hello.ann"/>
 */
//how spring come to know it is a java config class

@Configuration
@ComponentScan(basePackages = {"com.hello.java_conf"})
public class AppConfig {

}
