package com.hello.java_conf;

import org.springframework.stereotype.Component;


@Component(value = "metro")
public class Metro implements Vehical{
	public void move() {
		System.out.println("moving in a metro");
	}
}
