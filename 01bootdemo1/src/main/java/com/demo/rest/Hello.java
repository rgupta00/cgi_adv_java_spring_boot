package com.demo.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Hello {
	@GetMapping(path = "hello")
	public String sayHello(@RequestParam(name = "name")String name) {
		return "hello to spring boot, getting started "+ name;
	}

}
