package com.beanscope;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class DemoScopeAndLifeCycle {
	
	public static void main(String[] args) {
		//destory is only called if u register shutdown hook
		
		AbstractApplicationContext ctx=new ClassPathXmlApplicationContext("lifecycle.xml");
		ctx.registerShutdownHook();
		Foo foo=ctx.getBean("foo", Foo.class);
		
		System.out.println(foo.getValue());
		
	}

}
