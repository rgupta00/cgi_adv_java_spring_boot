package com.beanscope;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
@Component
public class Foo {
	@Value("my value")
	private String value;

	public Foo() {
		System.out.println("foo ctr is called... ");
	}
	//a method should be called just after creating the bean : initMethods
	@PostConstruct
	public void myInit() {
		System.out.println("myInit method is called...");
	}
	public String getValue() {
		System.out.println("get val is called");
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	//a method should be called just before  bean is going to be destroy : destoryMethods
	@PreDestroy
	public void myDestory() {
		System.out.println("myDestroy method is called...");
	}
}
