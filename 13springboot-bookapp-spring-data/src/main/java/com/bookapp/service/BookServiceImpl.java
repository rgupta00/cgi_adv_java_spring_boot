package com.bookapp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bookapp.dao.Book;
import com.bookapp.dao.BookDao;
import com.bookapp.dao.BookNotFoundException;

@Service
@Transactional
public class BookServiceImpl implements BookService {

	public BookDao bookDao;

	@Autowired
	public BookServiceImpl(BookDao bookDao) {
		this.bookDao = bookDao;
	}

	@Override
	public List<Book> getAllBooks() {
		return bookDao.findAll();
	}

	@Override
	public Book addBook(Book book) {
		bookDao.save(book);
		return book;
	}

	@Override
	public Book updateBook(int bookId, Book book) {
		Book bookToUpdate = getBookById(bookId);
		bookToUpdate.setPrice(book.getPrice());
		bookDao.save(bookToUpdate);

		return bookToUpdate;
	}

	@Override
	public Book deleteBook(int bookId) {
		Book bookToDelete = getBookById(bookId);
		bookDao.delete(bookToDelete);
		return bookToDelete;
	}

	@Override
	public Book getBookById(int bookId) {
		return bookDao.findById(bookId)
				.orElseThrow(() -> new BookNotFoundException("book with id: " + bookId + " is not found"));
	}

}
