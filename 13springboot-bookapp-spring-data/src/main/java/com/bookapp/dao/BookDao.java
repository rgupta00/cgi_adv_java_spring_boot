package com.bookapp.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
//CrudRepository
//JpaRepository

@Repository
public interface BookDao extends JpaRepository<Book, Integer>{
	public Book findByAuthor(String author);
}
