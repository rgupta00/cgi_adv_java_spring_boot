package com.bookapp.web.controller;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bookapp.dao.Book;
import com.bookapp.service.BookService;

@RestController
@RequestMapping(path = "api")
public class BookController {

	private BookService bookService;
	
	@Autowired
	public BookController(BookService bookService) {
		this.bookService = bookService;
	}
	//---------gettng all books------------
	@GetMapping(path = "book", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Book> getAllBooks(){
		return bookService.getAllBooks();
	}
	
	//---------gettng an specific book------------
	@GetMapping(path="book/{id}")
	public Book getById(@PathVariable(name = "id") int bookId) {
		return bookService.getBookById(bookId);
		
	}
	
	//---------add book------------
	
	@PostMapping(path="book",
			produces = MediaType.APPLICATION_JSON_VALUE
			,consumes = MediaType.APPLICATION_JSON_VALUE)
	public Book addBook(@RequestBody  Book book) {
		return bookService.addBook(book);
		
	}
	
	//---------delete book------------
	@DeleteMapping(path="book/{id}")
	public Book deleteBook (@PathVariable(name="id") int id) {
		return bookService.deleteBook(id);
	}
	
	//---------update book------------
	
	@PutMapping(path="book/{id}")
	public Book updateBook (@RequestBody Book book, @PathVariable(name="id") int id) {
		return bookService.updateBook(id, book);
	}
	
	
	
	@GetMapping(path = "hello")
	public String hello() {
		return "hello to spring rest";
	}
}
