package com.bookapp.model.dao;

import java.util.List;

import org.springframework.stereotype.Repository;
@Repository
public class BookDaoImpl implements BookDao{

	@Override
	public List<Book> getAllBooks() {
		return List.of(new Book(121, "AQ12", "java basics", "raj", 455.0),
				new Book(11, "AQM2", "Spring basics", "ekta", 455.0));
	}

	@Override
	public Book getBookById(int id) {
		return null;
	}

	@Override
	public Book addBook(Book book) {
		return null;
	}

	@Override
	public List<Book> getBookByTitle(String title) {
		return null;
	}

	@Override
	public Book updateBook(int bookId, Book book) {
		return null;
	}

	@Override
	public Book deleteBook(int bookId) {
		return null;
	}

}
