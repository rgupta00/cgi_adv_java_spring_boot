package com.bookapp;

import java.util.Arrays;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication

public class Application {

	public static void main(String[] args) {
		ApplicationContext ctx=  SpringApplication.run(Application.class, args);
		
//		String []beansNames=ctx.getBeanDefinitionNames();
//		Arrays.asList(beansNames).stream().forEach(name-> System.out.println(name));
	}

}
