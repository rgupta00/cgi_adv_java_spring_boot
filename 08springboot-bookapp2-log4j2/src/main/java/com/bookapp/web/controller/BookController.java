package com.bookapp.web.controller;
import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bookapp.model.dao.Book;
import com.bookapp.model.service.BookService;

@RestController
public class BookController {
	
	private BookService bookService;

	private Logger logger=LoggerFactory.getLogger(BookController.class);
	@Autowired
	public BookController(BookService bookService) {
		this.bookService = bookService;
	}
	@RequestMapping(path = "book", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Book> getAllBooks(){
		logger.info("hello world logging");
		return bookService.getAllBooks();
	}

}
