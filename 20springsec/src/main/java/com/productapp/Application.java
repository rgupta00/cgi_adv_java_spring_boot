package com.productapp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.productapp.dao.Product;
import com.productapp.dao.User;
import com.productapp.service.ProductService;
import com.productapp.service.UserService;

@SpringBootApplication
public class Application implements CommandLineRunner {

	@Autowired
	private ProductService productService;
	
	@Autowired
	private UserService userService;
	
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		
	Product product1=new Product("tv", 56, 12, 21,"raj");
		Product product2=new Product("laptop", 80, 20, 34,"ekta");
		Product product3=new Product("mouse", 1, 30, 200,"gunika");
		
//		productService.addProduct(product1);
//		productService.addProduct(product2);
//		productService.addProduct(product3);
		
		
		
		
		User user=new User("raj", "raj121", "ROLE_ADMIN");
		User user2=new User("ekta", "ekta121", "ROLE_MGR");
		User user3=new User("gunika", "gun121", "ROLE_EMP");
//		userService.addUser(user);
//		userService.addUser(user2);
//		userService.addUser(user3);
//		
		System.out.println("----user is added.......");
		
		
	}

}
