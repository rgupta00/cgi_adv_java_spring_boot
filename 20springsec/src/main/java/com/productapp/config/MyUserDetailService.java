package com.productapp.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.productapp.dao.User;
import com.productapp.service.UserService;

@Service
public class MyUserDetailService implements UserDetailsService {

	@Autowired
	private UserService userService;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user=userService.findByName(username);
		//i need to convert this user object to the spring security defined user
		System.out.println("----------------------");
		System.out.println(user.getName());
		System.out.println("----------------------");
		org.springframework.security.core.userdetails.User secUser=
				new org.springframework.security.core.userdetails.User
				(user.getName(), user.getPassword(), 
						AuthorityUtils.createAuthorityList(new String[] {user.getProfile()}));
		
		return secUser;
	}

}







