package com.productapp.config;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint;
import org.springframework.stereotype.Component;

@Component
public class MyAuthEntryPoint extends BasicAuthenticationEntryPoint {

	@Override
	public void commence(HttpServletRequest request, HttpServletResponse response,AuthenticationException authException) throws IOException {
		response.setHeader("WWW-authenticate", "basic relem"+getRealmName()+" ");
		response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
		PrintWriter out=response.getWriter();
		out.print("AUTHOERIZATION FAILED CONTANCT CGI");
		super.commence(request, response, authException);
	}

	@Override
	public void afterPropertiesSet() {
		setRealmName("productapp");
		super.afterPropertiesSet();
	}

	

}
