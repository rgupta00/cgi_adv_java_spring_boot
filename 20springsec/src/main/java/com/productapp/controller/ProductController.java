package com.productapp.controller;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.productapp.dao.Product;
import com.productapp.service.ProductService;

@RestController
public class ProductController {
	
	private ProductService productService;

	@Autowired
	public ProductController(ProductService productService) {
		this.productService = productService;
	}
	
	@GetMapping(path = "product")
	public List<Product>getAllProducts(){
		return productService.getAllProducts();	
	}

	@GetMapping(path = "product/{id}")
	public Product getAnProduct(@PathVariable(name = "id") int id){
		return productService.findById(id);
	}

	@PostMapping(path = "product")
	public Product addProduct(@RequestBody Product product) {
		return productService.addProduct(product);
	}
	
	@DeleteMapping(path = "product/{id}")
	public Product deleteAnProduct(@PathVariable(name = "id") int id){
		return productService.deleteProduct(id);
	}
	
	@PutMapping(path = "product/{id}")
	public Product addProduct(@PathVariable(name = "id") int id, @RequestBody Product product) {
		return productService.updateProduct(id, product);
	}

	
}




