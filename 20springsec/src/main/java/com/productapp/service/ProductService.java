package com.productapp.service;

import java.util.List;

import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;

import com.productapp.dao.Product;

public interface ProductService {
	@PreAuthorize("hasAuthority('ROLE_ADMIN') or hasAuthority('ROLE_MGR') or hasAuthority('ROLE_EMP')")
	public List<Product> getAllProducts();

	@PreAuthorize("hasAuthority('ROLE_ADMIN') or hasAuthority('ROLE_MGR')")
	public Product addProduct(Product product);

	@PreAuthorize("hasAuthority('ROLE_ADMIN')")
	public Product deleteProduct(int id);

	@PreAuthorize("hasAuthority('ROLE_ADMIN')")
	public Product updateProduct(int id, Product product);
	@PostAuthorize ("returnObject.ownerName == authentication.name")
	//@PreAuthorize("hasAuthority('ROLE_ADMIN') or hasAuthority('ROLE_MGR') or hasAuthority('ROLE_EMP')")
	public Product findById(int id);

}
