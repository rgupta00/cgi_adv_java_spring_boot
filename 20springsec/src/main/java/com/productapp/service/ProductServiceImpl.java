package com.productapp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.productapp.dao.Product;
import com.productapp.dao.ProductRepo;
@Service
@Transactional
public class ProductServiceImpl implements ProductService {

	@Autowired
	private ProductRepo productRepo;
	
	@Override
	public List<Product> getAllProducts() {
		return productRepo.findAll();
	}

	@Override
	public Product addProduct(Product product) {
		productRepo.save(product);
		return product;
	}

	@Override
	public Product deleteProduct(int id) {
		Product productToDelete=findById(id);
		productRepo.delete(productToDelete);
		return productToDelete;
	}

	@Override
	public Product updateProduct(int id, Product product) {
		Product productToUpdate=findById(id);
		productToUpdate.setPrice(product.getPrice());
		productToUpdate.setDiscount(product.getDiscount());
		productToUpdate.setQuantity(product.getQuantity());
		
		productRepo.save(productToUpdate);
		
		return productToUpdate;
		
	}

	@Override
	public Product findById(int id) {
		Product product=productRepo.findById(id)
				.orElseThrow(()-> new ProductNotFoundException("product with id "+ id+ " is not found"));
		return product;
	}

}
