package com.productapp.service;
import java.util.*;

import com.productapp.dao.User;
public interface UserService {
	public void addUser(User user);
	public User findByName(String name);
}
