package com.orderapp.consumer;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import com.orderapp.config.MessagingConfig;
import com.orderapp.dto.OrderStatus;

@Component
public class OrderConsumer {

    @RabbitListener(queues ="javaexp.queue")
    public void consumeMessageFromQueue(OrderStatus orderStatus) {
    	if(orderStatus.getOrder().getOrderId()==null)
    		throw new InvalidOrderException();
        System.out.println("Message recieved from queue : " + orderStatus);
    }
}