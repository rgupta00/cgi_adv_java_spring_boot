package com.bookapp.web;

import java.util.List;

import com.bookapp.model.dao.Book;
import com.bookapp.model.service.BookService;
import com.bookapp.model.service.BookServiceImpl;
public class Main {

	public static void main(String[] args) {
		
		BookService bookService=new BookServiceImpl();
		
		List<Book> books=bookService.getAllBooks();
		books.forEach(b-> System.out.println(b));
		
		
	}
}
