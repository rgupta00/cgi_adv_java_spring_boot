package com.bookapp.dao;
import java.util.*;
public interface BookDao {
	public List<Book> getAllBooks();
	public Book addBook(Book book);
	public Book updateBook(int bookId, Book book);
	public Book deleteBook(int bookId);
	public Book getBookById(int bookId);
}
