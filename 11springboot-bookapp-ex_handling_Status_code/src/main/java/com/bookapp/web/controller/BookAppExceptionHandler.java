package com.bookapp.web.controller;

import java.util.Date;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.bookapp.dao.BookNotFoundException;

@RestController
@ControllerAdvice
public class BookAppExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(BookNotFoundException.class)
	public ResponseEntity<ErrorDetails> handleBookNotFound(BookNotFoundException ex ,WebRequest request){
		ErrorDetails details=new ErrorDetails();
		details.setTimeStamp(new Date());
		details.setEmail("rgupta.mtech@gmail.com");
		details.setMessage(ex.getMessage());
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(details);
	}
	
	@ExceptionHandler(Exception.class)
	public ResponseEntity<ErrorDetails> handleOtherException(Exception ex ,WebRequest request){
		ErrorDetails details=new ErrorDetails();
		details.setTimeStamp(new Date());
		details.setEmail("rgupta.mtech@gmail.com");
		details.setMessage("some internal server error try after some time");
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(details);
	}

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		System.out.println("-------------------------");
		ErrorDetails details=new ErrorDetails(request.getDescription(false), new Date(), 
				"rgupta.mtech@gmail.com");
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(details);
	}
	
	
}
