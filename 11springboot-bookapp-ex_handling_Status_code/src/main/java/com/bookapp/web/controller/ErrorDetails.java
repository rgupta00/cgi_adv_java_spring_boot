package com.bookapp.web.controller;

import java.util.Date;

public class ErrorDetails {
	private String message;
	private Date timeStamp;
	private String email;
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Date getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(Date timeStamp) {
		this.timeStamp = timeStamp;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public ErrorDetails(String message, Date timeStamp, String email) {
		this.message = message;
		this.timeStamp = timeStamp;
		this.email = email;
	}
	public ErrorDetails() {}
	
	
	
}
