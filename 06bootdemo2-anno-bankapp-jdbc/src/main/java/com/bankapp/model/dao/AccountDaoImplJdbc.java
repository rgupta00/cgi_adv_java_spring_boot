package com.bankapp.model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;
@Primary
@Repository
public class AccountDaoImplJdbc implements AccountDao{
	
	private DataSource dataSource;
	

	public AccountDaoImplJdbc(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	@Override
	public List<Account> getAll() {
		List<Account> accounts=new ArrayList<Account>();
		Account account=null;
		try {
			Connection connection=dataSource.getConnection();
			
			PreparedStatement pstmt=connection.prepareStatement("select * from account_table");
			ResultSet rs=pstmt.executeQuery();
			while(rs.next()) {
				account=new Account(rs.getInt(1), rs.getString(2), rs.getDouble(3));
				accounts.add(account);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return accounts;
	}

	@Override
	public void update(Account account) {
		
	}

	@Override
	public Account getById(int id) {
		return null;
	}

}
