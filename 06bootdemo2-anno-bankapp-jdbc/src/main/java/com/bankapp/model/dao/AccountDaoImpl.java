package com.bankapp.model.dao;

import java.util.*;

import org.springframework.stereotype.Repository;

@Repository
public class AccountDaoImpl implements AccountDao{
	private Map<Integer, Account> accounts=new HashMap<Integer, Account>();
	
	{
		accounts.put(1, new Account(1,"raj",5000));
		accounts.put(2, new Account(2,"ekta",5000));
		
	}
	@Override
	public List<Account> getAll() {
		System.out.println("using hardcoded collection");
		return new ArrayList<Account>(accounts.values());
	}

	@Override
	public void update(Account account) {
		accounts.put(account.getId(), account);
	}

	@Override
	public Account getById(int id) {
		return accounts.get(id);
	}

}
