package com.bookapp.model.dao;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;
@Repository
@Primary
public class BookDaoImplJpa implements BookDao {

	private EntityManager em;
	
	@Autowired
	public BookDaoImplJpa(EntityManager em) {
		this.em = em;
	}

	@Override
	public List<Book> getAllBooks() {
		return em.createQuery("select b from Book b", Book.class).getResultList();
	}

	@Override
	public Book getBookById(int id) {
		Book book=em.find(Book.class, id);
		if(book==null)
			throw new BookNotFoundException();
		return book;
	}

	@Override
	public Book addBook(Book book) {
		em.persist(book);
		return book;
	}

	@Override
	public List<Book> getBookByTitle(String title) {
		return null;
	}

	@Override
	public Book updateBook(int bookId, Book book) {
		Book bookToUpdate=getBookById(bookId);
		bookToUpdate.setPrice(book.getPrice());
		em.merge(bookToUpdate);
		return bookToUpdate;
	}

	@Override
	public Book deleteBook(int bookId) {
		Book bookToDetele=getBookById(bookId);
		
		em.remove(bookToDetele);
		return bookToDetele;
	}

}
