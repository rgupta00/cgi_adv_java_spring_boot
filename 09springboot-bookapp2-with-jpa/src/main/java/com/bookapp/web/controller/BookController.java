package com.bookapp.web.controller;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.bookapp.model.dao.Book;
import com.bookapp.model.service.BookService;

@RestController
public class BookController {
	
	private BookService bookService;

	@Autowired
	public BookController(BookService bookService) {
		this.bookService = bookService;
	}
	@GetMapping(path = "book", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Book> getAllBooks(){
		return bookService.getAllBooks();
	}
	
	//@PathVariable: it is used to pass some path with url pattern
	//http://localhost:8080/book/1
	
	@GetMapping(path = "book/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public Book getAnBook(@PathVariable(name = "id") int bookId){
		return bookService.getBookById(bookId);
	}
	
	@PostMapping(path = "book",
			consumes =  MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public Book postAnBook(@RequestBody Book book){
		return bookService.addBook(book);
	}
	
	@PutMapping(path = "book/{id}",
			consumes =  MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public Book updateAnBook(@PathVariable(name = "id")int id,   @RequestBody Book book){
		return bookService.updateBook(id, book);
	}
	
	@DeleteMapping(path = "book/{id}",
			consumes =  MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public Book deleteAnBook(@PathVariable(name = "id")int id){
		return bookService.deleteBook(id);
	}
	
	
	
	//@RequestParam: is used the take data from the request ?
	//................/book?author=raj&rank=5
	

}
