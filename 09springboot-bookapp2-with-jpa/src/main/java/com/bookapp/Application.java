package com.bookapp;

import java.util.Arrays;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;

import com.bookapp.model.dao.Book;
import com.bookapp.model.service.BookService;

@SpringBootApplication
public class Application implements CommandLineRunner {

	@Autowired
	private BookService bookService;
	
	public static void main(String[] args) {
		ApplicationContext ctx=  SpringApplication.run(Application.class, args);

	}

	@Override
	public void run(String... args) throws Exception {
		
		//bookService.addBook(new Book("Z12", "life in action", "amit", 800));
		//bookService.addBook(new Book("AM1", "hibernate in action", "pqr", 700));
		
		//System.out.println("records are inserted...");
		
	}

}
