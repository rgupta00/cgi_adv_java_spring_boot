package com.bookapp.web.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bookapp.dao.FullName;
import com.bookapp.dao.Name;


@RestController
public class DemoVersionController {
	

	@GetMapping(path="name" ,produces  = "application/vnd.company.app-v1+json")
	public Name getName() {
		return new Name("raj");
	}
	@GetMapping(path="name", produces = "application/vnd.company.app-v2+json")
	public FullName getFullName() {
		return new FullName("raj","gupta");
	}
	
	
	
//	
//	@GetMapping(path="name" ,headers = "X-API-VERSION=1")
//	public Name getName() {
//		return new Name("raj");
//	}
//	@GetMapping(path="name", headers = "X-API-VERSION=2")
//	public FullName getFullName() {
//		return new FullName("raj","gupta");
//	}
//	
	
	
//	@GetMapping(path="name" ,params = "version=1")
//	public Name getName() {
//		return new Name("raj");
//	}
//	@GetMapping(path="name", params = "version=2")
//	public FullName getFullName() {
//		return new FullName("raj","gupta");
//	}
//	
	
	//neeraj, syed, ts
//	@GetMapping(path="v1/name")
//	public Name getName() {
//		return new Name("raj");
//	}
//	@GetMapping(path="v2/name")
//	public FullName getFullName() {
//		return new FullName("raj","gupta");
//	}
}
