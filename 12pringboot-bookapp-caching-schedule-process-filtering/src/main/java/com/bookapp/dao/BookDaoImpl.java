package com.bookapp.dao;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
@Repository
public class BookDaoImpl implements BookDao{
	
	private EntityManager em;

	@Autowired
	public BookDaoImpl(EntityManager em) {
		this.em = em;
	}

	@Override
	public List<Book> getAllBooks() {
		return em.createQuery("select b from Book b", Book.class).getResultList();
	}

	@Override
	public Book addBook(Book book) {
		em.persist(book);
		return book;
	}

	@Override
	public Book updateBook(int bookId, Book book) {
		Book bookToUpdate=getBookById(bookId);
		bookToUpdate.setPrice(book.getPrice());
		em.merge(bookToUpdate);
		return bookToUpdate;
	}

	@Override
	public Book deleteBook(int bookId) {
		Book bookToDelete=getBookById(bookId);
		em.remove(bookToDelete);
		return bookToDelete;
	}

	@Override
	public Book getBookById(int bookId) {
		Book book=em.find(Book.class, bookId);
		if(book==null) {
			throw new BookNotFoundException("book with id="+bookId+" is not found");
		}
		return book;
	}

}
