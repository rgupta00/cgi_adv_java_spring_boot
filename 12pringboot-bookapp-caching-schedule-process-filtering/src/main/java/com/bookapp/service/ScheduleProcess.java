package com.bookapp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class ScheduleProcess {
	
	private BookService bookService;

	@Autowired
	public ScheduleProcess(BookService bookService) {
		this.bookService = bookService;
	}
	//@Scheduled(cron = "0,30 * * * * *")
	@Scheduled(initialDelay = 1000, fixedRate = 30000)
	public void cronJob() {
		System.out.println("cache is cleaned...");
		bookService.cleanCache();
	}

}
