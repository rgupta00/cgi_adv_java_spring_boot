package com.bookapp.service;

import java.util.List;

import com.bookapp.dao.Book;

public interface BookService {
	public List<Book> getAllBooks();
	public Book addBook(Book book);
	public Book updateBook(int bookId, Book book);
	public Book deleteBook(int bookId);
	public Book getBookById(int bookId);
	public void cleanCache();
}
