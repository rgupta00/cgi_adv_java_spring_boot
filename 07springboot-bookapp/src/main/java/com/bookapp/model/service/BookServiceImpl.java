package com.bookapp.model.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bookapp.model.dao.Book;
import com.bookapp.model.dao.BookDao;
@Service
public class BookServiceImpl implements BookService {

	private BookDao bookDao;
	
	@Autowired
	public BookServiceImpl(BookDao bookDao) {
		this.bookDao = bookDao;
	}
	
	@Override
	public List<Book> getAllBooks() {
		return bookDao.getAllBooks();
	}

	@Override
	public Book getBookById(int id) {
		return null;
	}

	@Override
	public Book addBook(Book book) {
		return null;
	}

	@Override
	public List<Book> getBookByTitle(String title) {
		return null;
	}

	@Override
	public Book updateBook(int bookId, Book book) {
		return null;
	}

	@Override
	public Book deleteBook(int bookId) {
		return null;
	}

	

}
