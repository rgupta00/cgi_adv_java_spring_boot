package com.demo;

public class Book {
	private int id;
	private String titile;
	private double price;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitile() {
		return titile;
	}
	public void setTitile(String titile) {
		this.titile = titile;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public Book(int id, String titile, double price) {
		this.id = id;
		this.titile = titile;
		this.price = price;
	}
	public Book() {}
	
	
}
