package com.demo;
import java.util.*;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;


//adv of spring controller : they are testable and support di, aop and other modules of  spring framework
//@Controller
@RestController //@ResponseBody + @Controller
public class HelloController {

	@GetMapping(path = "hello")
	public String  sayHello() {
		return "spring mvc rest hello world";
	}
	
	@GetMapping(path = "book", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Book>  getAllBooks() {
		List<Book> books=List.of(new Book(121, "java basics", 300.6),new Book(11, "spring basics", 380.6));
		return books;
	}
	
}
